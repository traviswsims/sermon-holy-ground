# Holy Ground #

## Let's Talk About Moses ##
  - [ PICTURE ] **1956**
  - Brave (killed Egyptian, drove away shepherds)
  - Appreciated justice ^^
  - Wise, "mighty in words and deeds" **Acts 7:22**, Stephen says to Sanhedrin:

      > And Moses was instructed in all the wisdom of the Egyptians, and he was mighty in his words and deeds.

  - An exile
  - Not circumcised **Exodus 4**

### Exodus 2:11-17 ###

  > 11 One day, when Moses had grown up, he went out to his people and looked on their burdens, and he saw an Egyptian beating a Hebrew, one of his people.
  >
  > 12 He looked this way and that, and seeing no one, he struck down the Egyptian and hid him in the sand.
  >
  > 13 When he went out the next day, behold, two Hebrews were struggling together. And he said to the man in the wrong, "Why do you strike your companion?"
  >
  > 14 He answered, "Who made you a prince and a judge over us? Do you mean to kill me as you killed the Egyptian?" Then Moses was afraid, and thought, "Surely the thing is known."
  >
  > 15 When Pharaoh heard of it, he sought to kill Moses. But Moses fled from Pharaoh and stayed in the land of Midian. And he sat down by a well.
  >
  > 16 Now the priest of Midian had seven daughters, and they came and drew water and filled the troughs to water their father's flock.
  >
  > 17 The shepherds came and drove them away, but Moses stood up and saved them, and watered their flock.

## Moses at Horeb ##

### Exodus 3:1-6 ###

  > 3 Now Moses was keeping the flock of his father-in-law, Jethro, the priest of Midian, and he led his flock to the west side of the wilderness and came to Horeb, the mountain of God.
  >
  > 2 And the angel of the Lord appeared to him in a flame of fire out of the midst of a bush. He looked, and behold, the bush was burning, yet it was not consumed.
  >
  > 3 And Moses said, "I will turn aside to see this great sight, why the bush is not burned."
  >
  > 4 When the Lord saw that he turned aside to see, God called to him out of the bush, "Moses, Moses!" And he said, "Here I am."
  >
  > 5 Then he said, "Do not come near; take your sandals off your feet, for the place on which you are standing is holy ground."
  >
  > 6 And he said, "I am the God of your father, the God of Abraham, the God of Isaac, and the God of Jacob." And Moses hid his face, for he was afraid to look at God.



## Let's Talk About God ##

### Exodus 3:7-15 ###
  > 7 Then the Lord said, "**I have surely seen** the affliction of my people who are in Egypt and have heard their cry because of their taskmasters. I know their sufferings,
  >
  > 8 and **I have come down to deliver them** out of the hand of the Egyptians and to bring them up out of that land to a good and broad land, a land flowing with milk and honey, to the place of the Canaanites, the Hittites, the Amorites, the Perizzites, the Hivites, and the Jebusites.
  >
  > 9 And now, behold, the cry of the people of Israel has come to me, and I have also seen the oppression with which the Egyptians oppress them.
  >
  > 10 Come, **I will send you** to Pharaoh that you may bring my people, the children of Israel, out of Egypt."
  >
  > 11 But Moses said to God, "Who am I that I should go to Pharaoh and bring the children of Israel out of Egypt?"
  >
  > 12 He said, "But **I will be with you**, and this shall be the sign for you, that I have sent you: when you have brought the people out of Egypt, you shall serve God on this mountain."
  >
  > 13 Then Moses said to God, "If I come to the people of Israel and say to them, 'The God of your fathers has sent me to you,' and they ask me, 'What is his name?' what shall I say to them?"
  >
  > 14 God said to Moses, "**I am who I am.**" And he said, "Say this to the people of Israel, 'I am has sent me to you.'"
  >
  > 15 God also said to Moses, "Say this to the people of Israel, 'The Lord, the God of your fathers, the God of Abraham, the God of Isaac, and the God of Jacob, has sent me to you.' This is my name forever, **and thus I am to be remembered throughout all generations**.

### Key Points ###
- Perceptive "I have seen", "I have heard"
- Powerful "I have come down to deliver them (8)", I will be with you" (12)

### I AM WHO I AM ###
#### Joseph Benson's Commentary:  I AM WHO I AM ####
  > [This name] signifies,
  >
  > **First**, that he is self-existent: he has his being of
  > himself, and has no dependance on any other. And being self-existent, he
  > cannot but be self-sufficient, and therefore all-sufficient, and the
  > inexhaustible fountain of being and blessedness.
  >
  > **Second**, That he is eternal and
  > unchangeable: the same yesterday, to-day, and for ever.
  > Other beings are, and have been, and
  > shall be; but because what they have been might have been otherwise, and
  > what they are might possibly not have been at all, and what they shall be
  > may be very different from what now is therefore their changeable,
  > dependant, and precarious essence, which today may be one thing, tomorrow
  > another thing, and the next day possibly nothing at all, scarce
  > deserves the name of being

#### Hebrews 13:8 ####
  > Jesus Christ is the same yesterday and today and forever.

#### Paul: Acts 17:28 ####
  > In him we live and move and have our being



## Let's Talk About Us ##

### 1 Peter 2:1-12 ###
  >  1 So put away all malice and all deceit and hypocrisy and envy and all slander.
  >
  >  2 Like newborn infants, long for the pure spiritual milk, that by it you may grow up into salvation—
  >
  >  3 if indeed you have tasted that the Lord is good.
  >
  >  ...
  >
  >  9 But you are a chosen race, a royal priesthood, a holy nation, a people for his own possession, that you may proclaim the excellencies of him who called you out of darkness into his marvelous light.
  >
  >  10 **Once you were not a people, but now you are God's people**; once you had not received mercy, but now you have received mercy.
  >
  >  11 Beloved, I urge you as sojourners and exiles to abstain from the passions of the flesh, which wage war against your soul.
  >
  >  12 **Keep your conduct among the Gentiles honorable**, so that when they speak against you as evildoers, they may see your good deeds and glorify God on the day of visitation.

#### Key Points ####
- We are God's people
- Keep our conduct honorable -- How would Moses have acted in front of the Egyptians?
- So: abstain from the passions of the flesh


### Hebrews 10:19-24 ###

> 19 Therefore, brothers, since **we have confidence to enter the holy places by the blood of Jesus**,
>
> 20 by the new and living way that he opened for us through the curtain, that is, through his flesh,
>
> 21 and since we have a great priest over the house of God,
>
> 22 let us draw near with a true heart in full assurance of faith, with our hearts sprinkled clean from an evil conscience and our bodies washed with pure water.
>
> 23 Let us hold fast the confession of our hope without wavering, for he who promised is faithful.
>
> 24 And let us consider how to stir up one another to love and good works


## Let's Talk About You ##

### Non-Christians

- Moses had to go to the fire.  It didn't come to him.  **Acts 7:31** Stephen says,

  > When Moses saw it, he was amazed at the sight, and as he drew near to look, there came the voice of the Lord

### Christians: Exodus 4:24-26 (God to kill Moses)

  > 24 At a lodging place on the way the Lord met him and sought to put him to death.

  > 25 Then Zipporah took a flint and cut off her son's foreskin and touched Moses' feet with it and said, "Surely you are a bridegroom of blood to me!"

  > 26 So he let him alone. It was then that she said, "A bridegroom of blood," because of the circumcision.


  Perhaps this was the result of pressure from his surrogate Midianite tribe. It
  is also possible that he was persuaded by Zipporah not to circumcise his son,
  since she apparently found the practice revolting (4:25). This would explain her
  violent outburst; she felt that she had saved her husband from death by shedding
  the blood of her son. Whatever the cause, Moses' outstanding sin made him unfit
  to serve as a spiritual leader, and the situation had to be rectified before he
  could carry out his mission effectively. Indeed, as soon as Zipporah performed
  the act, the Lord "let him go." In summary, God was going to kill Moses because
  **Moses was supposed to teach the Israelites God's Law, yet Moses was not obeying
  God's Law himself.**
